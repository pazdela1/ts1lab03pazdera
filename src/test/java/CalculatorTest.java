import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator cal;

    @BeforeEach
    public void beforeEach() {
        cal = new Calculator();
    }

    @Test
    public void add_test() {
        int ret = cal.add(5, 5);

        Assertions.assertEquals(10, ret);
    }

    @Test
    public void subtract_test() {
        int ret = cal.subtract(5, 5);

        Assertions.assertEquals(0, ret);
    }

    @Test
    public void multiply_test() {
        int ret = cal.multiply(5, 5);

        Assertions.assertEquals(25, ret);
    }

    @Test
    public void divide_test() {
        int ret = cal.divide(5, 5);

        Assertions.assertEquals(1, ret);
    }

    @Test
    public void divide_divider_zero_test() {
        try {
            int ret = cal.divide(5, 0);
        } catch (Exception ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals("Division by 0", ex.getMessage());
        }
    }

}
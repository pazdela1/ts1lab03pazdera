import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

//@TestMethodOrder(MethodOrderer.Random.class)
//@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FooTest {


    Foo foo;


    @BeforeAll
    public static void beforeAll() {
        System.out.println("I am at the beginning of all tests.");
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("I am at the end of all tests.");
    }

    @BeforeEach
    public void beforeEach() {
        System.out.println("I setup things.");
        foo = new Foo();
    }

    @AfterEach
    public void afterEach() {
        System.out.println("I reset things.");
    }

    @Test
    @Order(2)
    public void returnNumber_returns_5() {
        int ret = foo.returnNumber();

        Assertions.assertEquals(5, ret);
    }

    @Test
    @Order(1)
    public void increment_increments_by_1() {
//        Foo foo = new Foo();
        foo.increment();

        Assertions.assertEquals(1, foo.getNum());
    }


}